public class Cars {
    protected int moves;
    protected final String id;

    public Cars(int moves, int id) {
        this.moves = moves;
        StringBuilder builder = new StringBuilder();
        String rowId = String.valueOf(id);
        if(id<=9999) {
            int countOfZero = 4 - rowId.length();
            for (int i = 0; i < countOfZero; i++) {
                builder.append("0");
            }
        }
        this.id = builder.append(rowId).toString();
    }

    public boolean checkTimeUp() {
        return this.moves <= 0;
    }
    public void spendTime(){
        this.moves -=1;
    }

    public int getMoves() {
        return moves;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName()+"{" +
                "moves=" + moves +
                ", id='" + id + '\'' +
                '}';
    }
}