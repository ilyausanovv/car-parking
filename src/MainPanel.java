import java.util.Scanner;

public class MainPanel {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
//normal car
        System.out.print("Enter space for normal cars: ");
        int carSpace = sc.nextInt();
//trucks
        System.out.print("\nEnter space for trucks: ");
        int truckSpace = sc.nextInt();
        CarParking carPark = new CarParking(carSpace, truckSpace);
        boolean keepRunning = true;
        while(keepRunning){
            System.out.println("Press any key to continue and press Q to stop");
            String s = sc.nextLine();
            if(s.equals("Q")||s.equals("q")){
                keepRunning = false;
            }else
                carPark.runTime();

        }
    }
}