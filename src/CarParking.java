import java.util.Random;

public class CarParking {
    public static int counter = 1;
    private static final int MOVES = 10;
    private final Cars[] cars;
    private final Truck[] trucks;
    private final int carSpaces;
    private final int truckSpaces;
    private final int sumSpaces;
    private int freeSpaces;
    private int occupiedSpaces;
    //constructor
    public CarParking(int carSpaces, int truckSpaces){
        this.carSpaces = carSpaces;
        this.truckSpaces = truckSpaces;
        this.sumSpaces = this.carSpaces+this.truckSpaces;
        cars = new Cars[this.carSpaces];
        trucks = new Truck[this.truckSpaces];
        this.freeSpaces = sumSpaces;
        this.occupiedSpaces = 0;
    }
    public void runTime(){
        Cars[] newCars = generateCar();
        Truck[] newTrucks = generateTrucks();
        parkNewCars(newCars,newTrucks);
        updateData();
        printParkSlots();
        spendCarTimes();
        checkCarTime();
    }
    public void updateData(){
        resetCounters();
        for (Cars car: this.cars){
            if(car==null){
                freeSpaces+=1;
            }else occupiedSpaces+=1;
        }
        for(Truck truck: this.trucks){
            if(truck==null){
                freeSpaces++;
            }else occupiedSpaces++;
        }
    }
    public void printParkSlots(){
        System.out.println("\n »»»»»»PARKING SLOTS«««««««");
        System.out.println("\n »»»»»»CAR SLOTS«««««««");
        for (int i = 0; i < cars.length; i++) {
            System.out.printf("%d)%s%n",i+1,(cars[i]==null)?" ":cars[i].toString());
        }
        System.out.println("\n »»»»»»TRUCK SLOTS«««««««");
        for (int i = 0; i < trucks.length; i++) {
            System.out.printf("%d)%s%n",i+1,(trucks[i]==null)?" ":trucks[i].toString());
        }
        System.out.println("DATA:\n\tFree Spaces: "+this.freeSpaces+"\tOccupied Spaces: "+this.occupiedSpaces);
    }
    public void spendCarTimes(){
        for(Cars c: this.cars){
            if(c!=null)c.spendTime();
        }
        for(Truck c: this.trucks){
            if(c!=null)c.spendTime();
        }
    }
    public void checkCarTime(){
        for (int i = 0; i < cars.length; i++) {
            if(cars[i]!=null&&cars[i].checkTimeUp())cars[i] = null;
        }
        for (int i = 0; i < trucks.length; i++) {
            if(trucks[i]!=null&&trucks[i].checkTimeUp())trucks[i] = null;
        }
    }
    public void resetCounters(){
        this.occupiedSpaces = 0;
        this.freeSpaces = 0;
    }
    public void parkNewCars(Cars[] incomingCars, Truck[] incomingTrucks){
        int enteredCars = 0;
        int enteredTrucks = 0;
        for (int i = 0; i < this.trucks.length; i++) {
            if(this.trucks[i]==null&&enteredTrucks!=incomingTrucks.length){
                this.trucks[i] = incomingTrucks[enteredTrucks++];
                System.out.println(this.trucks[i]+" has entered Truck space at place "+i);
            }
        }
        for (int i = 0; i < this.cars.length; i++) {
            boolean isLast = i == this.cars.length-1;
            if(!isLast) {
                if (this.cars[i] == null && this.cars[i + 1] == null&&enteredTrucks!=incomingTrucks.length){
                    this.cars[i] = incomingTrucks[enteredTrucks];
                    this.cars[i+1] = incomingTrucks[enteredTrucks++];
                    System.out.println(this.cars[i]+" has entered Car space at places"+i+" and "+i+1);
                    continue;
                }
            }
            if(this.cars[i]==null && enteredCars!=incomingCars.length){
                this.cars[i] = incomingCars[enteredCars++];
                System.out.println(this.cars[i]+" has been parked Car space at place "+i);
            }
        }
    }
    public static int generateMoves(){
        Random random = new Random();
        return random.nextInt(MOVES)+1;
    }
    public Truck[] generateTrucks(){
        Truck[] incomingTrucks = new Truck[sumSpaces/3];
        for (int i = 0; i < incomingTrucks.length; i++) {
            int moves = generateMoves();
            incomingTrucks[i] = new Truck(moves,counter++);
        }
        System.out.println("Incoming Trucks: ");
        for(Truck truck: incomingTrucks) System.out.println("\t"+truck);
        return incomingTrucks;
    }
    public Cars[] generateCar(){
        Cars[] incomingCars = new Cars[sumSpaces /2];
        for (int i = 0; i < incomingCars.length; i++) {
            int moves = generateMoves();
            incomingCars[i] = new Cars(moves,counter++);
        }
        System.out.println("Incoming Cars: ");
        for(Cars car: incomingCars) System.out.println("\t"+car.toString());
        return incomingCars;
    }
}